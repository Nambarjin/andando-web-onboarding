/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/anchor-has-content */
import React, { useState, useEffect } from 'react';
import './On-boarding.css';

function Step1() {
  return (
    <>
      <div className="step-top">
        <h1 className="on-boarding__header">
          ¿Como comenzar a andar? <br/> ¿Quieres aplicar para un crédito?
        </h1>
        <p className="card__text card__text--sub">
          Solo debes seguir estos sencillos pasos:
        </p>
      </div>
      <div className="on-boarding__cards">
        <div className="on-boarding__card">
            <div className="card__img-container">
                <img src="./assets/1.svg" alt=""/>
                <div className="card__list-number">1</div>
            </div>
            <p className="card__text">
              Regístrate para generar un perfil de manera automática.
            </p>
        </div>
        <div className="on-boarding__card">
            <div className="card__img-container">
                <img src="./assets/2.svg" alt=""/>
                <div className="card__list-number">2</div>  
            </div>
            <p className="card__text">
            Selecciona el tipo de crédito al que quieres aplicar y llena la solicitud.
            </p>
        </div>
        <div className="on-boarding__card">
            <div className="card__img-container">
                <img src="./assets/3.svg" alt=""/>
                <div className="card__list-number">3</div>
            </div>
            <p className="card__text">
            Prepárate para tu entrevista. Es tan fácil y rápido como una clase en línea. Hazla desde tu celular o computadora.
            </p>
        </div>
        <div className="on-boarding__card">
            <div className="card__img-container">
                <img src="./assets/4.svg" alt=""/>
                <div className="card__list-number">4</div>
            </div>
            <p className="card__text">
            Preséntate solo una vez en nuestras oficinas para firmar tu contrato.
            </p>
        </div>
        <div className="on-boarding__card">
            <div className="card__img-container">
                <img src="./assets/5.svg" alt=""/>
                <div className="card__list-number">5</div>
            </div>
            <p className="card__text">
            Recibe la transferencia y disfruta de tu crédito.x
            </p>
        </div>
      </div>
    </>
  )
}

function Step2() {
  return (
    <>
      <div className="step-top">
        <h1 className="on-boarding__header">
          ¿Que necesito?
        </h1>
        <p className="card__text card__text--sub">
        Los requisitos son muy simples, durante el proceso <br/> recuerda tener a lamano lo siguiente:
        </p>
      </div>
      <div className="on-boarding__cards">
        <div className="on-boarding__card just-center">
            <div className="card__img-container">
                <img src="./assets/6.svg" alt=""/>
                <div className="card__list-number">1</div>
            </div>
            <p className="card__text">
            Prepárate para tu entrevista. Es tan fácil y rápido como una clase en línea. Hazla desde tu celular o computadora.
            </p>
        </div>
        <div className="on-boarding__card just-center">
            <div className="card__img-container">
                <img src="./assets/7.svg" alt=""/>
                <div className="card__list-number">2</div>
            </div>
            <p className="card__text">
            Prepárate para tu entrevista. Es tan fácil y rápido como una clase en línea. Hazla desde tu celular o computadora.
            </p>
        </div>
        <div className="on-boarding__card just-center">
            <div className="card__img-container">
                <img src="./assets/4.svg" alt=""/>
                <div className="card__list-number">3</div>
            </div>
            <p className="card__text">
            Prepárate para tu entrevista. Es tan fácil y rápido como una clase en línea. Hazla desde tu celular o computadora.
            </p>
        </div>
      </div>
    </>
  )
}

function Step3() {
  return (
    <>
      <div className="step-top">
      <h1 className="on-boarding__header">
        ¿Dónde recibo mi dinero?
      </h1>
      <p className="card__text card__text--sub">
      Recibe directo en tu cuenta de banco.<br/><br/>
      Te recomendamos mucho bajar la app de tu banco en caso de no contar con ella.<br/>
      Así podrás monitorear tus movimientos y realizar pagos y transferencias.<br/>
      En caso de no contar con una cuenta de banco dejamos los links para obtener una de<br/><br/>
      manera fácil y rápida:</p>
      </div>
      <div className="list-items">
        <div className="list-item">
            <div className="list-item__img">
                <img src="./assets/logo-1.png" alt="" />
            </div>
            <div className="list-item__text">
                  <b className="list-item__bold">Básica Santander:</b>
                  <a className="list-item__link" href="https://www.santander.com.mx/personas/cuentas/cuenta-basica.html" target="_blank" rel="noopener noreferrer">https://www.santander.com.mx/personas/cuentas/cuenta-basica.html</a>
            </div>
        </div>
        <div className="list-item">
            <div className="list-item__img">
                <img src="./assets/logo-2.png" alt="" />
            </div>
            <div className="list-item__text">
                  <b className="list-item__bold">Fondeadora:</b>
                  <a className="list-item__link" href="https://fondeadora.com/" target="_blank" rel="noopener noreferrer">https://fondeadora.com/</a>
            </div>
        </div>
        <div className="list-item">
            <div className="list-item__img">
                <img src="./assets/logo-3.png" alt="" />
            </div>
            <div className="list-item__text">
                  <b className="list-item__bold">Cuenca:</b>
                  <a className="list-item__link" href="https://cuenca.com/" target="_blank" rel="noopener noreferrer">https://cuenca.com/</a>
            </div>
        </div>
        <div className="list-item">
            <div className="list-item__img">
                <img src="./assets/logo-4.png" alt="" />
            </div>
            <div className="list-item__text">
                  <b className="list-item__bold">Saldazo oxxo:</b>
                  <a className="list-item__link" href="https://www.banamex.com/transfer/obten-tarjeta-saldazo.html" target="_blank" rel="noopener noreferrer">https://www.banamex.com/transfer/obten-tarjeta-saldazo.html</a>
            </div>
        </div>
        <div className="list-item">
            <div className="list-item__img">
                <img src="./assets/logo-5.png" alt="" />
            </div>
            <div className="list-item__text">
                  <b className="list-item__bold">Banco azteca:</b>
                  <a className="list-item__link" href="https://www.bancoazteca.com.mx/productos/cuentas/basica.html" target="_blank" rel="noopener noreferrer">https://www.bancoazteca.com.mx/productos/cuentas/basica.html</a>
            </div>
        </div>
        <div className="list-item list-item--last">
            <p>De igual manera si no sabes como hacer transferencias desde tu <br/>app te dejamos este video tutorial:</p>
            <a href="#" className="btn btn--tutorial" target="_blank" rel="noopener noreferrer"><i className="fas fa-video"></i>Link de video tutorial</a>
        </div>
      </div>
    </>
  )
}

function Step4() {
  return (
    <>
      <div className="step-top">
      <h1 className="on-boarding__header">
        ¿Cómo pago mi crédito?
      </h1>
      <p className="card__text card__text--sub">
        Te damos las siguientes opciones de pago:
      </p>
      </div>
      <div className="step4-list">
          <div className="step4-list__item">
            <img src="./assets/blue-1.png" alt="" />
            <div className="step4-list__item__text">
                <b className="list-item__bold">Transferencia bancaria desde la app de tu banco</b>
                <p>(Nosotros te recomendamos esta opción ya que es la más rápida, practica y económica)</p>
            </div>
          </div>
          <div className="step4-list__item">
            <img src="./assets/blue-2.png" alt="" />
            <div className="step4-list__item__text">
                <b className="list-item__bold">Deposito en el banco</b>
            </div>
          </div>
          <div className="step4-list__item">
            <img src="./assets/blue-3.png" alt="" />
            <div className="step4-list__item__text">
                <b className="list-item__bold">Pago desde nuestra página web</b>
            </div>
          </div>
          <div className="step4-list__item">
            <img src="./assets/blue-4.png" alt="" />
            <div className="step4-list__item__text">
                <b className="list-item__bold">Pago Con tu tarjeta de crédito o debito en nuestra sucursal</b>
            </div>
          </div>
          <div className="step4-list__item">
            <img src="./assets/blue-5.png" alt="" />
            <div className="step4-list__item__text">
                <b className="list-item__bold">Depósitos en el Oxxo</b>
            </div>
          </div>
          <p className="card__text card__text--sub" style={{margin: '45px 0'}}>SI AUN TIENES DUDAS DIRIGETE A NUESTRA <br/>PAGINA DE PREGUNTAS FRECUENTES</p>
      </div>
    </>
  )
}

function App() {
  const [selectedStep, setSelectedStep] = useState(0)
  const [visited, setVisited] = useState('false')

  useEffect(() => {
      setVisited(localStorage.getItem('visitedOnboarding'))
  }, [])

  const clickNext = () => {
    setSelectedStep(selectedStep + 1)
  }

  const clickPrev = () => {
    if(selectedStep > 0)
      setSelectedStep(selectedStep - 1)
  }

  const getSlide = () => {
    if(selectedStep === 0) 
      return <Step1 />
    if(selectedStep === 1) 
      return <Step2 />
    if(selectedStep === 2) 
      return <Step3 />
    if(selectedStep === 3) 
      return <Step4 />
    if(selectedStep > 3) {
      setVisited(true)
      saveStatusLocalStorage()
    }
  } 

  const saveStatusLocalStorage = () => {
    localStorage.setItem('visitedOnboarding', visited);
  }

  return visited !== 'true' ? (
    <div className="on-boarding">
      <div className="container">
            { getSlide() }
              <div className="bottom-nav">
              { selectedStep > 0 && selectedStep <= 3 ?
                <a href="#" className="btn btn--prev" onClick={clickPrev}>Espalda</a> : 
                <a href="#" className="btn btn--prev" style={{opacity: '0', cursor: 'default'}}>Espalda</a>
              }
              { selectedStep <= 3 && (
                <>
                  <div className="nav-circles">
                    <div className={`nav__circle ${selectedStep === 0 && "nav__circle--active"}`} onClick={() => setSelectedStep(0)}></div>
                    <div className={`nav__circle ${selectedStep === 1 && "nav__circle--active"}`} onClick={() => setSelectedStep(1)}></div>
                    <div className={`nav__circle ${selectedStep === 2 && "nav__circle--active"}`} onClick={() => setSelectedStep(2)}></div>
                    <div className={`nav__circle ${selectedStep === 3 && "nav__circle--active"}`} onClick={() => setSelectedStep(3)}></div>
                  </div>
                  <a href="#" className="btn btn--next" onClick={clickNext}>Siguiente</a>
                </>
              )}
            </div>
      </div>
    </div>
  ): null
}

export default App;
